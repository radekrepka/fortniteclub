window.addEventListener("load", function () {
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#000"
            },
            "button": {
                "background": "#f1d600"
            }
        },
        "theme": "classic",
        "position": "bottom-left",
        "content": {
            "message": "Náš web ukládá při hlasování, přidávání teorii a komentářů tvoji ip do naší databáze, aby se minimalizovalo zmanipulování ankety a zamezilo spamu. Dále se uloží nějaké ty cookies #GDPR",
            "dismiss": "Souhlasím"
        }
    })
});