var modalForms = {};

$(function () {
    $('[data-modal-form-add]').click(function(){
        var $this = $(this);
        var formName = $this.attr('data-modal-form-add');
        var formSelector = '[modal-form=' + formName + ']';
        $(formSelector + " input[name=id]").val('0');
        $(formSelector + " input[name=addOrEdit]").val('add');
        var message = $(formSelector).attr('data-modal-form-message-add');
        vex.dialog.open({
            message: message,
            input: modalForms[formName],
            buttons: [
                $.extend({}, vex.dialog.buttons.YES, { text: 'Přidat' }),
                $.extend({}, vex.dialog.buttons.NO, { text: 'Zrušit' })
            ],
            callback: function (data) {
                if (data) {
                    for (var index in data) {
                        $(formSelector + " [name=" + index + "]").val(data[index]);
                    }
                    $(formSelector).submit();
                }
            }
        });
    });

    $('[data-modal-form-edit]').click(function(){
        var $this = $(this);
        var formName = $this.attr('data-modal-form-edit');
        var formSelector = '[modal-form=' + formName + ']';
        $(formSelector + " input[name=id]").val($(this).attr('data-modal-form-id'));
        $(formSelector + " input[name=addOrEdit]").val('edit');
        var message = $(formSelector).attr('data-modal-form-message-edit');
        vex.dialog.open({
            message: message,
            input: modalForms[formName],
            buttons: [
                $.extend({}, vex.dialog.buttons.YES, { text: 'Upravit' }),
                $.extend({}, vex.dialog.buttons.NO, { text: 'Zrušit' })
            ],
            afterOpen: function() {
                $('.vex-dialog-form input, .vex-dialog-form textarea').each(function(index) {
                    if($(this).attr('type') != 'file') {
                        var name = $(this).attr('name');
                        $(this).val($this.attr('data-modal-form-' + name));
                    }
                });
            },
            callback: function (data) {
                if (data) {
                    for (var index in data) {
                        $(formSelector + " [name=" + index + "]").val(data[index]);
                    }
                    $(formSelector).submit();
                }
            }
        });
    });

    $(document).on('change', '[modal-form-file-input]', function(){
        var $this = $(this);
        var $clone = $this.clone();
        var inputName = $this.attr('name');

        var formName = $this.attr("modal-form-file-input");
        var formSelector = '[modal-form=' + formName + ']';

        $(formSelector + ' input[name=' + inputName + ']').remove();
        $(formSelector).append($clone);
    });

    var formName = null;
    $('[modal-form] input, [modal-form] textarea').each(function(index) {
        if (formName == null)
            formName = $(this).closest("form").attr('modal-form');
        if (modalForms[formName] == null)
            modalForms[formName] = '';
        if ($(this).attr('type') != "hidden" && $(this).attr('type') != "submit") {
            var label = $('label[for=' + $(this).attr('id') + ']').text();
            var type = $(this).attr('type');
            var name = $(this).attr('name');
            var required = $(this).attr('required');

            if(required == null) {
                required = '';
            }

            if($(this).is('textarea')) {
                modalForms[formName] += label + ': <textarea name="' + name + '" placeholder="' + label + '" rows="9" ' + required + '></textarea>'
            }
            else {
                var isFileType = '';
                if (type == 'file') {
                    isFileType = 'modal-form-file-input="' + formName + '"';
                }
                modalForms[formName] += label + ': <input name="' + name + '" type="' + type + '" placeholder="' + label + '" ' + required + ' ' + isFileType + ' />';
            }
        }
    });
});