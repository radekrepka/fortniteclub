var backgroundImgUrl = basePath + "/assets/images/web/S5Map.png";
var center = [0, 0];
var map = L.map('map', {
    minZoom: 1,
    maxZoom: 4,
    center: center,
    zoom: 1,
    crs: L.CRS.Simple
});
// dimensions of the image
var w = 300, h = 300, url = backgroundImgUrl;
// calculate the edges of the image, in coordinate space
//var southWest = map.unproject([0, h], map.getMaxZoom() - 1);
//var northEast = map.unproject([w, 0], map.getMaxZoom() - 1);
//var bounds = new L.LatLngBounds(northEast, southWest);
// add the image overlay,
// so that it covers the entire map
L.imageOverlay(url, [[0, 0], [w, h]]).addTo(map);
// tell leaflet that the map is exactly as big as the image
map.setMaxBounds([[0, 0], [w, h]]);

coordinatesLoot = [];
coordinatesAmmo = [];
coordinatesVending = [];
coordinatesCart = [];

var loots = L.layerGroup();
var ammo = L.layerGroup();
var vmachines = L.layerGroup();
var carts = L.layerGroup();
var atks = L.layerGroup();
var rifts = L.layerGroup();
var apples = L.layerGroup();

var LootIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/loot_icon.png"
    }
});
var AmmoIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/ammo_icon.png"
    }
});
var VmachineIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/vmachine_icon.png"
    }
});
var CartIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/cart_icon.png"
    }
});
var AtkIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/atk_icon.png"
    }
});
var RiftIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/rift_icon.png"
    }
});
var AppleIcon = L.Icon.extend({
    options: {
        iconSize: [25, 32],
        iconUrl: basePath + "/assets/images/web/apple_icon.png"
    }
});
for (var i in markers) {
    var coordX = (markers[i].coordinates[0]) / 2048 * w;
    var coordY = (markers[i].coordinates[1]) / 2048 * h;
    var coods = [coordY, coordX];
    if (markers[i].type == 'loot') {
        coordinatesLoot.push(coods);
        //L.marker(coods, {icon: new LootIcon()}).addTo(map);
        L.marker(coods, {icon: new LootIcon()}).addTo(loots);
    }
    else if (markers[i].type == 'ammo') {
        //coordinatesAmmo.push(coods);
        L.marker(coods, {icon: new AmmoIcon()}).addTo(ammo);
    }
    else if (markers[i].type == 'vmachine') {
        //coordinatesVending.push(coods);
        L.marker(coods, {icon: new VmachineIcon()}).addTo(vmachines);
    }
    else if (markers[i].type == 'cart') {
        //coordinatesCart.push(coods);
        L.marker(coods, {icon: new CartIcon()}).addTo(carts);
    }
    else if (markers[i].type == 'atk') {
        //coordinatesCart.push(coods);
        L.marker(coods, {icon: new AtkIcon()}).addTo(atks);
    }
    else if (markers[i].type == 'rift') {
        //coordinatesCart.push(coods);
        L.marker(coods, {icon: new RiftIcon()}).addTo(rifts);
    }
    else if (markers[i].type == 'apple') {
        //coordinatesCart.push(coods);
        L.marker(coods, {icon: new AppleIcon()}).addTo(apples);
    }
}

var overlays = {
    'Truhly': loots.addTo(map),
    'Nákupní košíky': carts.addTo(map),
    'Vozidla': atks.addTo(map),
    'Portály': rifts.addTo(map),
    'HP': apples.addTo(map),
    'Prodejní automaty': vmachines,
    'Náboje': ammo
};

L.control.layers(null, overlays, {collapsed: false}).addTo(map);
