<?php

/**
 * Delete content of temp/ directory and/or Redis.
 * 	`php clear-cache.php [-r] [-t]`
 *
 * @param -r If given, will delete only Redis storage.
 * @param -t If given, will delete only temp/ content.
 */

$withTemp = in_array('-t', $argv) || count($argv) === 1;

// Delete content of temp file
if ($withTemp) {
	delete_directory(__DIR__ . DIRECTORY_SEPARATOR . 'temp');
}

function delete_directory($dirname, $delete_current_dir = false) {
	$skipFiles = ['.', '..', '.gitignore'];

	if (is_dir($dirname)) {
		$dir_handle = opendir($dirname);
	} else {
		return;
	}
	while ($file = readdir($dir_handle)) {
		if (!in_array($file, $skipFiles)) {
			$path = $dirname . DIRECTORY_SEPARATOR . $file;
			if (!is_dir($path)) unlink($path);
			else delete_directory($path, true);
		}
	}
	closedir($dir_handle);
	if ($delete_current_dir) {
		rmdir($dirname);
	}
	return;
}