#!/usr/bin/env bash
composer install
php ./www/index.php orm:schema-tool:update --dump-sql --force
php clear-cache.php

echo "[DONE] Branch is ready \n"
