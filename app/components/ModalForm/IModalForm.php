<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 17.07.2018
 * Time: 18:58
 */

namespace App\Components\ModalForm;


interface IModalForm
{
	/**
	 * @param $form
	 * @return ModalForm
	 */
	function create($form);
}