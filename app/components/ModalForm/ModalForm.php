<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 17.07.2018
 * Time: 18:54
 */

namespace App\Components\ModalForm;


use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ModalForm extends Control
{
	/** @var   */
	private $form;

	/** @var  string */
	private $messageAdd;

	/** @var  string */
	private $messageEdit;

	public function __construct($form) {
		parent::__construct();
		$this->form = $form;
	}

	public function render() {
		$this->template->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'modalForm.latte');
		$this->form->elementPrototype->addAttributes(['modal-form' => $this->getName()]);
		$this->form->elementPrototype->addAttributes(['data-modal-form-message-add' => $this->getMessageAdd()]);
		$this->form->elementPrototype->addAttributes(['data-modal-form-message-edit' => $this->getMessageEdit()]);
		$this->template->render();
	}

	public function createComponentModalForm() {
		return $this->form;
	}

	/**
	 * @param mixed $messageAdd
	 */
	public function setMessageAdd($messageAdd) {
		$this->messageAdd = $messageAdd;
	}

	/**
	 * @param string $messageEdit
	 */
	public function setMessageEdit($messageEdit) {
		$this->messageEdit = $messageEdit;
	}

	/**
	 * @return string
	 */
	public function getMessageAdd() {
		if($this->messageAdd == null) {
			$this->messageAdd = 'Přidat';
		}
		return $this->messageAdd;
	}

	/**
	 * @return string
	 */
	public function getMessageEdit() {
		if($this->messageEdit == null) {
			$this->messageEdit = 'Upravit';
		}
		return $this->messageEdit;
	}
}