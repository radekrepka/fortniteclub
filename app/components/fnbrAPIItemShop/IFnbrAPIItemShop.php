<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 01.07.2018
 * Time: 14:37
 */

namespace App\Components;


interface IFnbrAPIItemShop
{
	/** @return FnbrAPIItemShop */
	function create();
}