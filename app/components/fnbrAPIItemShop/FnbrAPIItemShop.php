<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 01.07.2018
 * Time: 14:32
 */

namespace App\Components;


use App\Models\FnbrAPI;
use Nette\Application\UI\Control;

class FnbrAPIItemShop extends Control
{
	/** @var  FnbrAPI */
	protected $fnbrAPI;

	public function __construct()
	{
		parent::__construct();
		$this->fnbrAPI = new FnbrAPI();
	}

	public function render()
	{
		$template = $this->template;
		$template->shopItems = array_merge($this->fnbrAPI->getItemShop()->data->featured, $this->fnbrAPI->getItemShop()->data->daily);
		$template->setFile(__DIR__ . '/itemShop.latte');
		$template->render();
	}
}