<?php

namespace App\Presenters;

use App\Components\ModalForm\IModalForm;
use App\Factories\FortniteNewFormFactory;
use Nette;
use Tracy\Debugger;

class TestsPresenter extends BasePresenter {

	/** @var  FortniteNewFormFactory @inject */
	public $addFortniteNewFormFactory;

	public function renderDefault() {
		Debugger::barDump($this->fortniteTrackerApi->getPlayer('Ninja'));
	}

	public function createComponentModalForm() {
		return $this->modalForm->create($this->addFortniteNewFormFactory->create());
	}
}
