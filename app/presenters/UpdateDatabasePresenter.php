<?php

namespace App\Presenters;


use Doctrine\DBAL\Exception\ConnectionException;
use Nette;
use Doctrine\ORM\Tools\SchemaTool;

class UpdateDatabasePresenter extends Nette\Application\UI\Presenter
{
	/** @var \Kdyby\Doctrine\EntityManager @inject*/
	public $entityManager;

	public function startup() {
		parent::startup();
		$metadatas = $this->entityManager->getMetadataFactory()->getAllMetadata();
		try {
			$schemaTool = new SchemaTool($this->entityManager);
			$schemaTool->updateSchema($metadatas, true);
			echo "done";
		} catch (ConnectionException $i) {
			$this->addMessage($i->getMessage());
			echo $i->getMessage();
		}
		die();
	}
}
