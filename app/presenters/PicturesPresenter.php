<?php

namespace App\Presenters;

use App\Models\Entities\LobbyRoom;
use App\Models\Entities\RoomChat;
use App\Models\Entities\Story;
use App\Models\FortniteApi\FortniteClient;
use App\Models\GalleryManager;
use Nette;

use App\Models\FnbrAPI;
use Nette\Application\UI\Form;
use Symfony\Component\Config\Definition\Exception\Exception;
use Tracy\Debugger;

class PicturesPresenter extends BasePresenter
{
	/** @var  \App\Models\GalleryManager @inject*/
	public $galleryManager;

	public function renderDefault() {
		$this->template->pictures = $this->galleryManager->getPictures();
	}
}
