<?php

namespace App\Presenters;

use App\Models\Entities\LobbyRoom;
use App\Models\Entities\RoomChat;
use App\Models\FortniteApi\FortniteClient;
use Nette;

use App\Models\FnbrAPI;
use Nette\Application\UI\Form;
use Symfony\Component\Config\Definition\Exception\Exception;
use Tracy\Debugger;

class LobbyPresenter extends BasePresenter
{
	public function renderDefault(){
		$this->template->rooms = $this->entityManager->getRepository(LobbyRoom::class)->findAll();
	}

	public function renderRoom($id){
		$this->template->roomId = $id;
		$this->template->messages = $this->entityManager->getRepository(RoomChat::class)
			->findBy(['id_room' => $id]);
	}

	public function handleRefreshChat($messageCount, $roomId){
		if ($this->isAjax()) {
			$this->redrawControl("chat");
		} else {
			$this->redirect("this");
		}
//		$dbMessageCount = $this->entityManager->getRepository(RoomChats::class)
//			->countBy(['id_room' => $roomId]);
//
//		Debugger::fireLog($dbMessageCount);
//		if($messageCount < $dbMessageCount){
//			$this->payload->messageCount = $dbMessageCount;
//			$this->redrawControl("chat");
//		}

	}

	public function createComponentChatForm(){
		$form = new Form;
		$form->addText('message')
			->setRequired();
		$form->addHidden('id_room');
		$form->addHidden('user');
		$form->addSubmit('submit');
		$form->onSuccess[] = function(Form $form, $values){
			$message = new RoomChat();
			$message->id_room = $values->id_room;
			$message->user = $values->user;
			$message->message = $values->message;

			$this->entityManager->persist($message);
			$this->entityManager->flush();

			$this->redrawControl("chat");
		};
		return $form;
	}

	public function createComponentCreateRoomForm(){
		$form = new Form;
		$form->addHidden('title')
			->setRequired();
		$form->addHidden('user')
			->setRequired();
		$form->addHidden('description')
			->setRequired();
		$form->addSelect('mode', null, [
			'solo' => 'solo',
			'duo' => 'duo',
			'squad' => 'squad'
		]);
		$form->addSubmit('submit');
		$form->onSuccess[] = function(Form $form, $values){
			$room = new LobbyRoom();
			$room->creator_name = $values->user;
			$room->title = $values->title;
			$room->description = $values->description;

			$this->entityManager->persist($room);
			$this->entityManager->flush();
		};
		return $form;
	}
}
