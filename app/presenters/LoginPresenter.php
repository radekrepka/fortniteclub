<?php

namespace App\Presenters;

use App\Models\FortniteApi\FortniteClient;
use Nette;

use App\Models\FnbrAPI;
use Nette\Application\UI\Form;
use Symfony\Component\Config\Definition\Exception\Exception;
use Tracy\Debugger;

class LoginPresenter extends BasePresenter
{
	public function renderDefault(){

	}

	public function createComponentLoginForm() {
		$form = new Form();
		$form->addText("username", "Login: ")
			->setRequired('Prosím vyplňte své uživatelské jméno.');
		$form->addPassword("password", "Heslo: ")
			->setRequired('Prosím vyplňte své heslo.');
		$form->addSubmit("submit", "Přihlásit se");
		$form->onSuccess[] = function(Form $form, $values){
			try{
				$this->getUser()->login($values->username, $values->password);
				$this->redirect(":Homepage:");
			}
			catch(Nette\Security\AuthenticationException $e){
				$this->flashMessage($e->getMessage());
				$form->addError('Nesprávné přihlašovací jméno nebo heslo.');
			}
		};
		return $form;
	}
}
