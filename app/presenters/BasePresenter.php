<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 20.06.2018
 * Time: 21:11
 */

namespace App\Presenters;

use App\Components\FnbrAPIItemShop;
use App\Components\IFnbrAPIItemShop;
use App\Components\ModalForm\ModalForm;
use App\Models\Entities\StoryComment;
use App\Models\FortniteApi\FortniteApiManager;
use App\Models\FortniteApi\FortniteClient;
use App\Models\FnbrAPI;
use App\Models\FortniteTrackerApi;
use App\Models\ModulesManager;
use App\Components\ModalForm\IModalForm;
use Nette;
use Tracy\Debugger;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/** @var \Kdyby\Doctrine\EntityManager @inject*/
	public $entityManager;

	/** @var  FnbrAPI */
	protected $fnbrAPI;

	/** @var FortniteTrackerApi @inject */
	public $fortniteTrackerApi;

	/** @var \App\Models\FortniteApi\FortniteClient */
	protected $fortniteClient;

	/** @var \App\Models\FortniteApi\FortniteApiManager @inject*/
	public $fortniteApiManager;

	/** @var  IFnbrAPIItemShop @inject */
	public $fnbrAPIItemShop;

	/** @var  ModulesManager @inject */
	public $modulesManager;

	/** @var  IModalForm @inject */
	public $modalForm;

	/** @var   */
	public $basePath;

	protected function startup(){
		parent::startup();
		$this->template->version = 24;
		$this->basePath = $this->getHttpRequest()->getUrl()->getBasePath();

		$modules = $this->modulesManager->getModules();

		$this->template->modules = $modules;

		if(array_key_exists($this->getName(), $modules)) {
			$this->template->title = $modules[$this->getName()]->name;
		}

		$this->fortniteClient = new FortniteClient('cbf47afe79d592e38f444f4c9ebfbe29');
		$this->fnbrAPI = new FnbrAPI();

		if(!$this->isAjax()){
			//$this->template->shopItems = $this->fortniteApiManager->getStore()->items;
			//$this->template->shopItems = $this->fortniteClient->items->store()->items;
			$this->template->shopItems = (new FnbrAPI())->getItemShop();
		}
	}


	public function handleSearchPlayer($username){
		$uid = $this->fortniteClient->user->id($username);
		if($this->isAjax()){
			if($uid == 'unknown_epic_user'){
				$this->payload->unknownEpicUser = true;
				$this->sendPayload();
			}
			else{
				$this->redirect("PlayerProfile:default", $username);
				//$this->payload->results = $this->fortniteClient->user->id($username);
			}
		}
		else{
			$this->redirect("this");
		}
	}

	public function handleLogout() {
		$this->getUser()->logout();
		$this->redirect('this');
	}

	public function createComponentFnbrItemShop(){
		return $this->fnbrAPIItemShop->create();
	}
}