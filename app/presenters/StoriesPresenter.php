<?php

namespace App\Presenters;

use App\Facades\StoryCommentFacade;
use App\Facades\StoryFacade;
use App\Facades\VoteHistoryFacade;
use App\Models\Entities\LobbyRoom;
use App\Models\Entities\RoomChat;
use App\Models\Entities\Story;
use App\Models\Entities\StoryComment;
use App\Models\Entities\VoteHistory;
use App\Models\FortniteApi\FortniteClient;
use DateInterval;
use Nette;

use App\Models\FnbrAPI;
use Nette\Application\UI\Form;
use Symfony\Component\Config\Definition\Exception\Exception;
use Tracy\Debugger;

class StoriesPresenter extends BasePresenter
{
	/** @var  StoryFacade @inject */
	public $storyFacade;

	/** @var  StoryCommentFacade @inject */
	public $storyCommentFacade;

	/** @var  VoteHistoryFacade @inject */
	public $voteHistoryFacade;

	public function renderDefault() {
		if(!isset($this->template->stories)) {
			$this->setStories('best');
		}

		if(!isset($this->template->voteStories)) {
			$this->template->voteStories = $this->entityManager->getRepository(VoteHistory::class)->getVoteStories($this->getHttpRequest()->getRemoteAddress());
		}

		if(!isset($this->template->sort)) {
			$this->template->sort = 'best';
		}
	}

	public function createComponentStoryForm() {
		$form = new Form;
		$form->addText('title', 'Název teorie')
			->setRequired()
			->addRule(Form::MAX_LENGTH, 'Text je příliš dlouhý', 50);
		$form->addText('user', 'Jméno hráče')
			->setRequired()
			->addRule(Form::MAX_LENGTH, 'Text je příliš dlouhý', 20);
		$form->addTextArea('body', 'Obsah')
			->setRequired()
			->addRule(Form::MAX_LENGTH, 'Text je příliš dlouhý', 1000000);
		$form->addSubmit('submit');
		$form->onSuccess[] = function (Form $form, $values) {
			if($this->storyFacade->canAdd($this->getHttpRequest()->getRemoteAddress())) {
				$story = new Story();
				$story->creator_name = $values->user;
				$story->title = $values->title;
				$story->body = $values->body;
				$story->creator_ip = $this->getHttpRequest()->getRemoteAddress();

				$this->entityManager->persist($story);
				$this->entityManager->flush();
			}
		};
		$modalForm = $this->modalForm->create($form);
		$modalForm->setMessageAdd('Napiš svou teorii');
		return $modalForm;
	}

	public function createComponentCommentStory(){
		$form = new Form;
		$form->addText('user')
			->setRequired()
			->addRule(Form::MAX_LENGTH, 'Text je příliš dlouhý', 50);
		$form->addText('body')
			->setRequired()
			->addRule(Form::MAX_LENGTH, 'Text je příliš dlouhý', 1000000);
		$form->addHidden('idStory')
			->setRequired();
		//$form->addReCaptcha('recaptcha', $label = 'Captcha')
		//	->setMessage('Are you bot?');
		$form->addSubmit('submit');
		$form->onSuccess[] = function (Form $form, $values) {
			$currentStory = $this->entityManager->getRepository(Story::class)->find($values->idStory);

			if($this->storyCommentFacade->canComment($currentStory, $this->getHttpRequest()->getRemoteAddress())) {
				$comment = new StoryComment();
				$comment->creator_name = $values->user;
				$comment->body = $values->body;
				$comment->story = $currentStory;
				$comment->creator_ip =  $this->getHttpRequest()->getRemoteAddress();

				$this->entityManager->persist($comment);
				$this->entityManager->flush();
			}

			if($this->isAjax()) {
				$this->setStoryComments($currentStory);
			}
		};
		return $form;
	}

	public function handleSort($sort) {
		$this->setStories($sort);
	}

	public function handleVoteStory($idStory, $voteType, $sort, $id) {
		if ($this->isAjax()) {
			if (!$this->voteHistoryFacade->isVoted($idStory, $this->getHttpRequest()->getRemoteAddress())) {
				if ($voteType == 'up') {
					$this->storyFacade->voteUp($idStory);
				}
//				elseif ($voteType == 'down') {
//					$this->storyFacade->voteDown($idStory);
//				}
				else {
					$this->redirect('this');
				}

				$voteHistory = new VoteHistory();
				$voteHistory->id_story = $idStory;
				$voteHistory->vote_type = $voteType;
				$voteHistory->ip = $this->getHttpRequest()->getRemoteAddress();
				$this->entityManager->persist($voteHistory);
				$this->entityManager->flush();

				$this->setStories($sort);
				//$this->template->stories[$id] = $this->entityManager->getRepository(Story::class)->find($idStory);
				$this->redrawControl('storyContainer');
			}
		}
		else {
			$this->redirect('this');
		}
	}

	public function handleBanStory($idStory){
		if ($this->getUser()->isLoggedIn()) {
			$story = $this->entityManager->getRepository(Story::class)->find($idStory);
			$this->entityManager->remove($story);
			$this->entityManager->flush();
		}
		else {
			$this->redirect('this');
		}
	}

	public function handleGetStoryComments($idStory) {
		if ($this->isAjax()) {
			$currentStory = $this->entityManager->getRepository(Story::class)->find($idStory);
			$this->setStoryComments($currentStory);
		}
		else {
			$this->redirect('this');
		}
	}

	/**
	 * @param $sort
	 */
	private function setStories($sort) {
		if($sort == 'best') {
			$this->template->stories = $this->entityManager->getRepository(Story::class)->findBy([], ['votes_likes' => 'desc']);
			$this->template->sort = 'best';
		}
		else if($sort == 'new') {
			$this->template->stories = $this->entityManager->getRepository(Story::class)->findBy([], ['time' => 'desc']);
			$this->template->sort = 'new';
		}
		else {
			$this->template->stories = [];
			$this->template->sort = 'best';
		}
	}

	/**
	 * @param $story
	 */
	private function setStoryComments($story) {
		//$currentStory = $this->entityManager->getRepository(Story::class)->find($idStory);
		$this->template->storyComments = $this->entityManager->getRepository(StoryComment::class)->findBy(['story' => $story], ['time' => 'ASC']);
		$this->template->currentStory = $story;
		$this->payload->currentStory = $story;
		$this->redrawControl('storyComments');
	}
}
