<?php

namespace App\Presenters;

use App\Components\ModalForm\IFortniteNewCollection;
use App\Facades\FortniteNewFacade;
use App\Factories\FortniteNewFormFactory;
use App\Models\Entities\FortniteNew;
use Nette;
use Tracy\Debugger;

class HomepagePresenter extends BasePresenter {

	const NEWS_LIMIT = 10;

	/** @var  FortniteNewFormFactory @inject */
	public $fortniteNewFormFactory;

	/** @var  FortniteNewFacade @inject */
	public $fortniteNewFacade;

	protected $offset = 0;

	public function renderDefault(){
		$this->template->news = $this->entityManager
			->getRepository(FortniteNew::class)
			->findBy(
				[],
				['api_time' => 'desc'],
				self::NEWS_LIMIT,
				$this->offset * self::NEWS_LIMIT
			);
		$this->template->newsOffset = $this->offset + 1;

		if($this->getUser()->isLoggedIn()) {
			$apiNews = $this->fortniteClient->news->get()->entries;
//			$apiNews = $this->fortniteApiManager->getNews()->entries;
			$this->template->apiNews = $this->fortniteNewFacade->getUnsavedNews($apiNews);
		}
	}

	public function handleAppendNews($offset) {
		if ($this->isAjax()) {
			$this->offset = $offset;
			$this->redrawControl('news');
			$this->redrawControl('newsOffsetLink');
		}
		else {
			$this->redirect('this');
		}
	}

	public function createComponentFortniteNewForm(){
		$newForm = $this->fortniteNewFormFactory->create();
		$modalForm = $this->modalForm->create($newForm);
		$modalForm->setMessageAdd('Přidat novinku');
		$modalForm->setMessageEdit('Upravit novinku');
		return $modalForm;
	}

	public function handleDeleteNew($idNew) {
		if($this->getUser()->isLoggedIn()) {
			$new = $this->entityManager->getRepository(FortniteNew::class)->find($idNew);
			$this->entityManager->remove($new);
			$this->entityManager->flush();
		}
		$this->redirect('this');
	}

	public function handleSortUp($idNew) {
		if($this->getUser()->isLoggedIn()) {
			$new = $this->entityManager->getRepository(FortniteNew::class)->find($idNew);
			$this->fortniteNewFacade->sortUp($new);
		}
		$this->redirect('this');
	}

	public function handleSortDown($idNew) {
		if($this->getUser()->isLoggedIn()) {
			$new = $this->entityManager->getRepository(FortniteNew::class)->find($idNew);
			$this->fortniteNewFacade->sortDown($new);
		}
		$this->redirect('this');
	}
}
