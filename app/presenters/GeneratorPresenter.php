<?php

namespace App\Presenters;

use App\Facades\ChallengeFacade;
use App\Models\Entities\Challenge;
use App\Models\FortniteApi\FortniteClient;
use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class GeneratorPresenter extends BasePresenter
{
	/** @var  ChallengeFacade @inject */
	public $challengeFacade;

	public function renderDefault(){
		if($this->getUser()->isLoggedIn()) {
			$this->template->challenges = $this->entityManager->getRepository(Challenge::class)->findAll();
		}
	}

	public function renderChallenge(){
		$this->template->challenge = $this->challengeFacade->getRandomChallenge();
	}

	public function createComponentChallengeForm(){
		$form = new Form;
		$form->addText('title', 'Title')
			->setRequired('Vyplň název');
		$form->addTextArea('body', 'Body')
			->setRequired('Vyplň text');
		$form->addHidden('addOrEdit')
			->setRequired();
		$form->addHidden('id')
			->setDefaultValue('0');
		$form->addSubmit('submit');
		$form->onSuccess[] = function(Form $form, $values){
			if($this->getUser()->isLoggedIn()) {
				if($values->addOrEdit == 'edit') {
					$challenge = $this->entityManager->getRepository(Challenge::class)->find($values->id);
				}
				else {
					$challenge = new Challenge();
				}
				$challenge->title = $values->title;
				$challenge->body = $values->body;

				$this->entityManager->persist($challenge);
				$this->entityManager->flush();
			}
		};
		return $this->modalForm->create($form);
	}

	public function handleDeleteChallenge($idChallenge) {
		if($this->getUser()->isLoggedIn()) {
			$challenge = $this->entityManager->getRepository(Challenge::class)->find($idChallenge);
			$this->entityManager->remove($challenge);
			$this->entityManager->flush();
		}
		$this->redirect('this');
	}
}
