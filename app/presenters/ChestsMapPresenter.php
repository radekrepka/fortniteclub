<?php

namespace App\Presenters;

use App\Models\FortniteApi\FortniteClient;
use App\Models\FortniteChestsApi;
use Nette;

use App\Models\FnbrAPI;
use Symfony\Component\Config\Definition\Exception\Exception;
use Tracy\Debugger;

class ChestsMapPresenter extends BasePresenter
{
	public function renderDefault(){
		$this->template->markers = FortniteChestsApi::get();
	}
}
