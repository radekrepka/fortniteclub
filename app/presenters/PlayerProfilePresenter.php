<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 20.06.2018
 * Time: 21:10
 */

namespace App\Presenters;

use Nette;

class PlayerProfilePresenter extends BasePresenter
{
	public function renderDefault($id){
		$this->fortniteClient->user->id($id);
		$this->template->playerProfie = $this->fortniteClient->user->stats();
	}
}