<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 23.06.2018
 * Time: 0:14
 */

namespace App\Models\Repositories;

use App\Models\Entities\RoomChat;
use App\Models\Entities\VoteHistory;
use Kdyby\Doctrine\EntityRepository;

class VoteHistoryRepository extends EntityRepository
{
	/**
	 * @param $ip
	 * @return array
	 */
	public function getVoteStories($ip) {
		$voteStories = $this->_em->getRepository(VoteHistory::class)
			->findBy(['ip' => $ip]);
		$voteStoriesArray = [];
		foreach($voteStories as $voteStory) {
			$voteStoriesArray[$voteStory->id_story] = $voteStory;
		}
		return $voteStoriesArray;
	}
}