<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 23.06.2018
 * Time: 0:14
 */

namespace App\Models\Repositories;

use App\Models\Entities\RoomChat;
use Kdyby\Doctrine\EntityRepository;

class RoomChatsRepository extends EntityRepository
{
	public function getRoomChat($roomId){
		$entityManager = $this->_em;

	}

	public function messageCount($roomId){
		return $this->_em->createQueryBuilder()
			->select("COUNT(m) as messageCount")
			->from(RoomChat::class, "m")
			->where('id_room = :id_room')
			->setParameter("id_room", $roomId)
			->getQuery()
			->getSingleScalarResult();
	}
}