<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 19.06.2018
 * Time: 13:39
 */

namespace App\Models\FortniteApi;


class FortniteWeapons
{

	public function Fortnite_Weapons($client)
	{
		$this->Client = $client;
	}
	public function get()
	{
		$return = json_decode($this->Client->httpCall('weapons/get', []));
		if(isset($return->error))
		{
			return $return->errorMessage;
		}
		else
		{
			return $return;
		}
	}
}