<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 19.06.2018
 * Time: 13:38
 */

namespace App\Models\FortniteApi;


class FortnitePVE
{

	public function __construct($client)
	{
		$this->Client = $client;
	}
	/*
	 * Get the current PVE info.
	 */
	public function info()
	{
		$return = json_decode($this->Client->httpCall('pveinfo/get', []));
		if(isset($return->error))
		{
			return $return->errorMessage;
		}
		else
		{
			return $return;
		}
	}
}