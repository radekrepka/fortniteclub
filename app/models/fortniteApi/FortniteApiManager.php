<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 23.06.2018
 * Time: 12:32
 */

namespace App\Models\FortniteApi;

use Nette;
use Nette\Caching\Cache;
use Tracy\Debugger;

class FortniteApiManager
{
	/** @var FortniteClient  */
	private $fortniteClient;

	/** @var Cache  */
	private $cache;

	public function __construct($tempDir) {
		$this->fortniteClient = new FortniteClient('cbf47afe79d592e38f444f4c9ebfbe29');

		$storage = new Nette\Caching\Storages\FileStorage($tempDir);
		$this->cache = new Cache($storage);
	}

	public function getStore(){
		$storeCache = $this->cache->load('store', function (&$dependencies) {
			Debugger::fireLog("Cache neexistuje");
			return $this->saveCache();
		});
		if($storeCache->date != date('d-n-y') && date('H') > 2){
			Debugger::fireLog("Nova cache");
			return $this->saveCache();
		}
		else{
			Debugger::fireLog("Dnesni cache");
			return $storeCache;
		}
	}

	public function getNews(){
		return $this->cache->load('fortniteNews', function (&$dependencies) {
			$store = $this->fortniteClient->news->get();
			$this->cache->save("fortniteNews", $store);
			return $store;
		});
	}

	private function saveCache(){
		$store = $this->fortniteClient->items->store();
		$this->cache->save("store", $store);
		return $store;
	}
}