<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 19.06.2018
 * Time: 13:35
 */

namespace App\Models\FortniteApi;


class FortniteClient
{
	private $client_version = 3.0;
	private $api_endpoint = 'https://fortnite-public-api.theapinetwork.com/prod09/';
	private $api_version = 'v1';
	private $pem_path;
	protected $ch;
	public $auth;
	public $challenges;
	public $leaderboard;
	public $items;
	public $news;
	public $patchnotes;
	public $pve;
	public $status;
	public $weapons;
	public $user;


	public function __construct($key = '')
	{
		$this->auth = new FortniteAuth($this);
		$this->challenges = new FortniteChallenges($this);
		$this->leaderboard = new FortniteLeaderboard($this);
		$this->items = new FortniteItems($this);
		$this->news = new FortniteNews($this);
		$this->patchnotes = new FortnitePatchNotes($this);
		$this->pve = new FortnitePVE($this);
		$this->status = new FortniteStatus($this);
		$this->weapons = new FortniteWeapons($this);
		$this->user = new FortniteUser($this);
		$this->pem_path = realpath(dirname(__FILE__) . '/cacert.pem');
		$this->setKey($key);
	}
	public function setKey($key = '')
	{
		$this->auth->setKey($key);
	}

	public static function Exception($err = '')
	{
		die($err);
	}
	public function httpCall($method = '', $fields = '', $custom = false)
	{
		if(empty($this->auth->auth))
		{
			FortniteClient::Exception('You have not set an API key. Use setKey() to set the API key.');
		}
		if(empty($this->ch) || !function_exists('curl_reset'))
		{
			$this->ch = curl_init();
		}
		else
		{
			curl_reset($this->ch);
		}
		if($custom == false) {
			$url = $this->api_endpoint . $method;
		} else {
			$url = $method;
		}
		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
		$headers = [
			'Authorization: ' . $this->auth->auth,
			'X-Fortnite-API-Version: ' . $this->api_version,
			'X-Fortnite-Client-Info: ' . php_uname(),
			'X-Fortnite-Client-Version: ' . $this->client_version,
		];
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($fields));
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($this->ch, CURLOPT_CAINFO, $this->pem_path);
		$body = curl_exec($this->ch);
		$this->response_code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
		if(curl_errno($this->ch))
		{
			$msg = 'Unable to communicate with Fortnite API (' . curl_errno($this->ch) . '): ' . curl_error($this->ch) . '.';
			$this->unsetHttpCall();
			FortniteClient::Exception($msg);
		}
		if(!function_exists('curl_reset'))
		{
			$this->unsetHttpCall();
		}
		if($this->response_code != 200)
		{
			FortniteClient::Exception('Something wen\'t wrong. We couldn\'t give you an 200 header back. - ' . $this->response_code);
		}
		return $body;
	}
	private function unsetHttpCall()
	{
		if(is_resource($this->ch))
		{
			curl_close($this->ch);
			$this->ch = null;
		}
	}
	public function __destruct()
	{
		$this->unsetHttpCall();
	}
}