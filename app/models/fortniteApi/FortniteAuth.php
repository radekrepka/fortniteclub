<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 19.06.2018
 * Time: 13:37
 */

namespace App\Models\FortniteApi;


class FortniteAuth
{

	public $auth;

	public function __construct($client)
	{
		$this->Client = $client;
	}
	public function setKey($key = '')
	{
		if(!empty($key))
		{
			$this->auth = $key;
			return '';
		}
		FortniteClient::Exception('Invalid API key.');
	}
}