<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 19.06.2018
 * Time: 13:38
 */

namespace App\Models\FortniteApi;


class FortnitePatchNotes
{

	public function __construct($client)
	{
		$this->Client = $client;
	}
	/*
	 * Patchnotes
	 */
	public function get()
	{
		$return = json_decode($this->Client->httpCall('patchnotes/get', []));
		if(isset($return->error))
		{
			return $return->errorMessage;
		}
		else
		{
			return $return;
		}
	}
}