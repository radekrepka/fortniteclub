<?php
namespace App\Models\Authenticators;

use Nette;
use Nette\Security as NS;

class Authenticator implements NS\IAuthenticator
{
    /**
     * Authenticator constructor.
     */
    function __construct(){

    }

    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

		$USERNAME = 'admin';
		$PASSWORD = 'spidermanokurka321';

        if ($username !== $USERNAME) {
            throw new NS\AuthenticationException('Uživatel nenalezen.');
        }

        //if (!NS\Passwords::verify($password, $user->password)) {
        if ($password !== $PASSWORD) {
            throw new NS\AuthenticationException('Špatné heslo.');
        }
        $role = "admin";

        return new NS\Identity($username, $role);
    }
}