<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 06.07.2018
 * Time: 17:29
 */

namespace App\Models;


use Nette\Utils\Finder;

class GalleryManager
{
	private $imgPath;

	public function __construct($imgPath) {
		$this->imgPath = $imgPath;
	}

	/**
	 * @return array
	 */
	public function getPictures(){
		$pictures = [];
		foreach (Finder::findFiles('*.jpg', '*.JPG', '*.png', '*.jpeg', '*.JPEG')->in($this->imgPath) as $key => $file) {
			$pictures[$key] = $file;
		}
		return $pictures;
	}
}