<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 08.07.2018
 * Time: 10:05
 */

namespace App\Facades;


use App\Models\Entities\Story;
use App\Models\Entities\StoryComment;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;

class StoryCommentFacade
{
	/** @var EntityManager  */
	private $entityManager;

	public function __construct(EntityManager $entityManager){
		$this->entityManager = $entityManager;
	}

	/**
	 * @param $story
	 * @param $ip
	 * @return bool
	 */
	public function canComment($story, $ip) {
		$date = new \DateTime();
		$date->sub(new \DateInterval('P0M0DT0H1M0S'));

		$result = $this->entityManager->createQueryBuilder()
			->select('COUNT(sc) as comment_count')
			->from(StoryComment::class, 'sc')
			->where('sc.time > :time')
			->andWhere('sc.story = :story')
			->andWhere('sc.creator_ip = :ip')
			->setParameter('time', $date)
			->setParameter('story', $story)
			->setParameter('ip', $ip)
			->getQuery()
			->getSingleScalarResult();

		return $result <= 0;
	}
}