<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 08.07.2018
 * Time: 10:05
 */

namespace App\Facades;


use App\Models\Entities\Story;
use Kdyby\Doctrine\EntityManager;

class StoryFacade
{
	/** @var EntityManager  */
	private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * @param $idStory
	 */
	public function voteUp($idStory) {
		$this->entityManager->createQuery("UPDATE " . Story::class ." s SET s.votes_likes = s.votes_likes + 1 WHERE s.id = :id")
			->setParameter('id', $idStory)
			->execute();
	}

	/**
	 * @param $idStory
	 */
	public function voteDown($idStory) {
		$this->entityManager->createQuery("UPDATE " . Story::class ." s SET s.votes_dislikes = s.votes_dislikes + 1 WHERE s.id = :id")
			->setParameter('id', $idStory)
			->execute();
	}

	/**
	 * @param $ip
	 * @return bool
	 */
	public function canAdd($ip) {
		$date = new \DateTime();
		$date->sub(new \DateInterval('P0M1DT0H0M0S'));

		$result = $this->entityManager->createQueryBuilder()
			->select('COUNT(s) as story_count')
			->from(Story::class, 's')
			->where('s.time > :time')
			->andWhere('s.creator_ip = :ip')
			->setParameter('time', $date)
			->setParameter('ip', $ip)
			->getQuery()
			->getSingleScalarResult();

		return $result <= 0;
	}
}