<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 08.07.2018
 * Time: 10:05
 */

namespace App\Facades;

use App\Models\Entities\FortniteNew;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;

class FortniteNewFacade
{
	/** @var EntityManager  */
	private $entityManager;

	private $savedNewsApiTime = null;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	public function getUnsavedNews($apiNews) {
		$unsavedNews = [];

		foreach ($apiNews as $apiNew) {
			if(!$this->isSaved($apiNew)) {
				$unsavedNews[] = $apiNew;
			}
		}
		return $unsavedNews;
	}

	public function isSaved($new) {
		return in_array($new->time, $this->getSavedNewsApiTime());
	}


	public function getSavedNewsApiTime() {
		if($this->savedNewsApiTime == null){
			$result = $this->entityManager->createQueryBuilder()
				->select('fn.api_time as api_time')
				->from(FortniteNew::class, 'fn')
				->where("fn.api_time != 'NULL'")
				->getQuery()
				->getResult();
			$temp = [];
			foreach($result as $resultItem) {
				$temp[] = $resultItem['api_time'];
			}
			$this->savedNewsApiTime = $temp;
		}
		return $this->savedNewsApiTime;
	}


	public function getNewSort() {
		return $this->entityManager->createQueryBuilder()
			->select('MAX(fn.sort)')
			->from(FortniteNew::class, 'fn')
			->getQuery()
			->getSingleScalarResult() + 1;
	}

	public function sortUp(FortniteNew $new) {
		$oldNew = $this->entityManager->getRepository(FortniteNew::class)->findOneBy(['sort' => $new->sort + 1]);
		if($oldNew != null) {
			$new->sort += 1;
			$oldNew->sort -= 1;

			$this->entityManager->persist($oldNew);
			$this->entityManager->persist($new);
			$this->entityManager->flush();
		}
	}

	public function sortDown(FortniteNew $new) {
		$oldNew = $this->entityManager->getRepository(FortniteNew::class)->findOneBy(['sort' => $new->sort - 1]);
		if($oldNew != null) {
			$new->sort -= 1;
			$oldNew->sort += 1;

			$this->entityManager->persist($oldNew);
			$this->entityManager->persist($new);
			$this->entityManager->flush();
		}
	}
}