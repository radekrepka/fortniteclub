<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 08.07.2018
 * Time: 10:05
 */

namespace App\Facades;


use App\Models\Entities\Challenge;
use App\Models\Entities\Story;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;

class ChallengeFacade
{
	/** @var EntityManager  */
	private $entityManager;

	public function __construct(EntityManager $entityManager) {
		$this->entityManager = $entityManager;
	}

	/**
	 * @return mixed
	 */
	public function getRandomChallenge() {
		$challenges = $this->entityManager->getRepository(Challenge::class)->findAll();
		return $challenges[array_rand($challenges)];
	}
}