<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 08.07.2018
 * Time: 10:05
 */

namespace App\Facades;


use App\Models\Entities\VoteHistory;
use Kdyby\Doctrine\EntityManager;

class VoteHistoryFacade
{
	/** @var EntityManager  */
	private $entityManager;

	/**
	 * VoteHistoryFacade constructor.
	 * @param EntityManager $entityManager
	 */
	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * @param $idStory
	 * @param $ip
	 * @return bool
	 */
	public function isVoted($idStory, $ip) {
		$count = $this->entityManager->createQueryBuilder()
			->select('COUNT(vh) as vote_count')
			->from(VoteHistory::class, "vh")
			->where('vh.id_story = :idStory')
			->andWhere('vh.ip = :ip')
			->setParameter('idStory', $idStory)
			->setParameter('ip', $ip)
			->getQuery()
			->getSingleScalarResult();
		return $count > 0;
	}
}