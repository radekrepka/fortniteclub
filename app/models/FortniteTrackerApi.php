<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 03.08.2018
 * Time: 23:32
 */

namespace App\Models;

use Nette;
use Nette\Caching\Cache;

class FortniteTrackerApi {

	const API_URL = 'https://api.fortnitetracker.com/v1/profile/pc/';

	/** @var Cache  */
	private $cache;

	public function __construct($tempDir) {
		$storage = new Nette\Caching\Storages\FileStorage($tempDir);
		$this->cache = new Cache($storage);
	}

	/**
	 * @param $url
	 * @return mixed
	 * @throws Nette\Utils\JsonException
	 */
	private function getJson($url){
		$opts = [
			"http" => [
				"method" => "GET",
				"header" =>
					"TRN-Api-Key: 732bbb4e-675d-487b-bac5-ec8cf9c2a08f\r\n"
			]
		];

		$context = stream_context_create($opts);
		$file = file_get_contents($url, false, $context);
		return Nette\Utils\Json::decode($file);
	}

	/**
	 * @param $playerName
	 * @return mixed
	 * @throws Nette\Utils\JsonException
	 */
	public function getPlayer($playerName) {
		$last = $this->cache->load('fortniteTrackerLastTime', function (&$dependencies) {
			$time = microtime(true);
			$this->cache->save("fortniteTrackerLastTime", $time);
			return $time;
		});
		$now = microtime(true);
		$difference = $now - $last;
		if ($difference < 2) {
			usleep(($difference + 0.2) * 1000);
		}
		return $this->getJson(self::API_URL . $playerName);
	}
}