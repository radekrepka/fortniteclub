<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.06.2018
 * Time: 23:23
 */

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="story_comment")
 */
class StoryComment extends BaseEntity
{
	public function __construct()
	{
		$this->time = new \DateTime();
	}

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	public $id;

	/**
	 * @manyToOne(targetEntity="Story")
	 * @joinColumn(name="id_story", referencedColumnName="id")
	 */
	public $story;

	/**
	 * @ORM\Column(name="body", type="string")
	 */
	public $body;

	/**
	 * @ORM\Column(name="creator_name", type="string")
	 */
	public $creator_name;

	/**
	 * @ORM\Column(name="creator_ip", type="string")
	 */
	public $creator_ip;

	/**
	 * @ORM\Column(name="time", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	public $time;
}