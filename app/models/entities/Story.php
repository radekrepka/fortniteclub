<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.06.2018
 * Time: 23:23
 */

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\StoryRepository")
 * @ORM\Table(name="story")
 */
class Story extends BaseEntity
{
	public function __construct()
	{
		$this->time = new \DateTime();
	}

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	public $id;

	/**
	 * @ORM\Column(name="title", type="string")
	 */
	public $title;

	/**
	 * @ORM\Column(name="body", type="text")
	 */
	public $body;

	/**
	 * @ORM\Column(name="votes_likes", type="integer")
	 */
	public $votes_likes = 0;

	/**
	 * @ORM\Column(name="votes_dislikes", type="integer")
	 */
	public $votes_dislikes = 0;

	/**
	 * @ORM\Column(name="creator_name", type="string")
	 */
	public $creator_name;

	/**
	 * @ORM\Column(name="creator_ip", type="string")
	 */
	public $creator_ip;

	/**
	 * @ORM\Column(name="time", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	public $time;
}