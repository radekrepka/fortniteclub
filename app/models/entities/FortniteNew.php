<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.06.2018
 * Time: 23:23
 */

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\FortniteNewRepository")
 * @ORM\Table(name="fortnite_new")
 */
class FortniteNew extends BaseEntity
{
	public function __construct()
	{
		$this->time = new \DateTime();
	}

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	public $id;

	/**
	 * @ORM\Column(name="title", type="string")
	 */
	public $title;

	/**
	 * @ORM\Column(name="body", type="string")
	 */
	public $body;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	public $image = null;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	public $api_time = null;

	/**
	 * @ORM\Column(type="integer")
	 */
	public $sort;

	/**
	 * @ORM\Column(name="time", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	public $time;
}