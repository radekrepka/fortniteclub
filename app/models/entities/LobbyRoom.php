<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.06.2018
 * Time: 23:23
 */

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="lobby_room")
 */
class LobbyRoom extends BaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	public $id;

	/**
	 * @ORM\Column(name="title", type="string")
	 */
	public $title;

	/**
	 * @ORM\Column(name="creator_name", type="string")
	 */
	public $creator_name;

	/**
	 * @ORM\Column(name="description", type="string")
	 */
	public $description;
}