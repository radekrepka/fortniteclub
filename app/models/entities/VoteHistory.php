<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 07.07.2018
 * Time: 13:38
 */

namespace App\Models\Entities;


use Kdyby\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\VoteHistoryRepository")
 * @ORM\Table(name="vote_history")
 */
class VoteHistory extends BaseEntity
{
	public function __construct()
	{
		$this->time = new \DateTime();
	}

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	public $id;

	/**
	 * @ORM\Column(name="id_story", type="integer")
	 */
	public $id_story;

	/**
	 * @ORM\Column(name="vote_type", type="string")
	 */
	public $vote_type;

	/**
	 * @ORM\Column(name="ip", type="string")
	 */
	public $ip;

	/**
	 * @ORM\Column(name="time", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	public $time;
}