<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.06.2018
 * Time: 23:23
 */

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\RoomChatsRepository")
 * @ORM\Table(name="room_chat")
 */
class RoomChat extends BaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	public $id;

	/**
	 * @ORM\Column(name="user", type="string")
	 */
	public $user;

	/**
	 * @ORM\Column(name="id_room", type="string")
	 */
	public $id_room;

	/**
	 * @ORM\Column(name="message", type="string")
	 */
	public $message;
}