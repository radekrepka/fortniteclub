<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 10.07.2018
 * Time: 20:21
 */

namespace App\Models;

use Nette;
use Nette\Caching\Cache;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;

class ModulesManager
{
	private $cache;

	private $modules;

	public function __construct() {
		$storage = new Nette\Caching\Storages\FileStorage(__DIR__.'/../../temp/cache');
		$this->cache = new Cache($storage);
	}

	/**
	 * @return mixed
	 */
	public function getModules() {
		if($this->modules == null) {
			$this->modules = $this->cache->load('routes', function (&$dependencies) {
				$configFile = __DIR__ . '/../config/modules.neon';
				$fileContent = FileSystem::read($configFile);
				$neonArray = [];
				try {
					$neonArray = Neon::decode($fileContent);
				} catch (Nette\Neon\Exception $e) {
					// zpracování výjimky
				}
				$routes = [];
				foreach($neonArray as $key => $module) {
					$routes[$key] = (object)[
						'name' => $module,
						'url' => Nette\Utils\Strings::webalize($module)
					];
				}
				$this->cache->save("routes", $routes);
				return $routes;
			});
		}
		return $this->modules;
	}
}