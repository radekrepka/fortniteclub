<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 17.06.2018
 * Time: 11:01
 */

namespace App\Models;

use Nette;

class FnbrAPI
{
	/** @var   */
	private $stats;

	/** @var   */
	private $itemShop;

	/**
	 * @param $url
	 * @return mixed
	 * @throws Nette\Utils\JsonException
	 */
	private function getJson($url){
		$opts = [
			"http" => [
				"method" => "GET",
				"header" =>
				//"Accept-language: en\r\n" .
				//"Cookie: foo=bar\r\n" .
					"x-api-key: d02a74cd-cc67-4032-95b8-97c155cbfdc4\r\n"
			]
		];

		$context = stream_context_create($opts);
		$file = file_get_contents($url, false, $context);
		return Nette\Utils\Json::decode($file);
	}

	/**
	 * @return mixed
	 * @throws Nette\Utils\JsonException
	 */
	public function getStats(){
		if($this->stats == null){
			$this->stats = $this->getJson("https://fnbr.co/api/stats");
		}
		return $this->stats;
	}

	/**
	 * @return mixed
	 * @throws Nette\Utils\JsonException
	 */
	public function getItemShop(){
		if($this->itemShop == null){
			$this->itemShop = $this->getJson("https://fnbr.co/api/shop");
		}
		return $this->itemShop;
	}


}