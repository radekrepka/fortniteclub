<?php

namespace App;

use App\Models\ModulesManager;
use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\Caching\Cache;
use Tracy\Debugger;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$moduleManager = new ModulesManager();
		foreach($moduleManager->getModules() as $key => $module) {
			if($key != 'Homepage') {
				$router[] = new Route($module->url, $key . ':default');
			}
		}
		$router[] = new Route('lobby/<id>', 'Lobby:room');
		$router[] = new Route('profile/<id>', [
			'presenter' => [
				Route::VALUE => 'PlayerProfile'
			],
			'action' => [
				Route::VALUE => 'default'
			],
			'id' => null,
		]);
		$router[] = new Route('challenge-generator/mapa', 'Generator:map');
		$router[] = new Route('challenge-generator/challenge', 'Generator:challenge');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}
}
