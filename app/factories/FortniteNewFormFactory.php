<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 17.07.2018
 * Time: 19:09
 */

namespace App\Factories;


use App\Facades\FortniteNewFacade;
use App\Models\Entities\FortniteNew;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;

class FortniteNewFormFactory
{
	/** @var   */
	private $imageDir;

	/** @var  EntityManager */
	private $entityManager;

	/** @var  User */
	private $user;

	/** @var FortniteNewFacade  */
	private $fortniteNewFacade;

	public function __construct($imageDir, EntityManager $entityManager, User $user, FortniteNewFacade $fortniteNewFacade) {
		$this->imageDir = $imageDir;
		$this->entityManager = $entityManager;
		$this->user = $user;
		$this->fortniteNewFacade = $fortniteNewFacade;
	}

	public function create() {
		$form = new Form;
		$form->addText('title', 'Title')
			->setRequired('Vyplň název');
		$form->addTextArea('body', 'Body')
			->setRequired('Vyplň text');
		$form->addUpload('image', 'Obrázek');
		$form->addHidden('apiImage');
		$form->addHidden('apiTime');
		$form->addHidden('addOrEdit')
			->setRequired();
		$form->addHidden('id')
			->setDefaultValue('0')
			->setRequired();
		$form->addSubmit('submit');
		$form->onError[] = function(Form $form) {
			Debugger::barDump($form->getValues());
			Debugger::barDump($form->getErrors());
		};
		$form->onSuccess[] = function(Form $form, $values){
			Debugger::barDump('form');
			if($this->user->isLoggedIn()) {
				if($values->addOrEdit == 'edit') {
					$fortniteNew = $this->entityManager->getRepository(FortniteNew::class)->find($values->id);
				}
				else {
					$fortniteNew = new FortniteNew();
				}
				$fortniteNew->title = $values->title;
				$fortniteNew->body = $values->body;
				$fortniteNew->sort = $this->fortniteNewFacade->getNewSort();

				if($values->apiTime != null) {
					$fortniteNew->api_time = $values->apiTime;
				}

				if($values->apiImage != null) {
					$fortniteNew->image = $values->apiImage;
				}

				$this->entityManager->persist($fortniteNew);
				$this->entityManager->flush();

				if ($values->image->getName() !== null) {
					//TODO uložit s basepath
					$imagePath = "assets/images/news/" . $fortniteNew->getId() . '_' . $values->image->getName();
					$values->image->move($this->imageDir . DIRECTORY_SEPARATOR . $imagePath);

					$fortniteNew->image = $imagePath;
					$this->entityManager->persist($fortniteNew);
					$this->entityManager->flush();
				}
			}
		};
		return $form;
	}
}